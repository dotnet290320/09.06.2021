using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using WebApplication2;

namespace TestProject1
{
    [TestClass]
    public class WeatherControllerTests
    {
        private readonly TestHostFixture _testHostFixture = new TestHostFixture();// Initializes the webHost
        private HttpClient _httpClient;//Http client used to send requests to the contoller

        [TestInitialize]
        public async Task SetUp()
        {
            _httpClient = _testHostFixture.Client;
        }

        [TestMethod]
        public async Task Search_Flight_With_No_Query_Parameters()
        {
            // add : POST PUT DELETE
            var response = await _httpClient.GetAsync("WeatherForecast");

            var responseContent = await response.Content.ReadAsStringAsync();

            // different serialization
            List<WeatherForecast> weatherForecastListResult = 
                JsonSerializer.Deserialize<List<WeatherForecast>>(responseContent, 
                new JsonSerializerOptions { PropertyNameCaseInsensitive = true });

            Assert.AreEqual(weatherForecastListResult.Count, 5);
        }
    }
}
